# Pythongo

Minimal MongoDB OOP client based on motor package.

## Example

Play with the example below for start learning.

```py
import pythongo
import asyncio


class People(pythongo.Model):
    def __init__(self, name, age) -> None:
        self.name = name
        self.age = age

        self.database = "company"
        self.collection = "peoples"

        super().__init__()


async def main():
    await pythongo.connect("mongodb://localhost:27017/")

    while True:
        name = input("Please enter a name: ")
        age = input("Please enter the age: ")

        user = People(name, pythongo.Ignore)

        if (await user.find_one()) is not None:
            print("This user is already exists!")
            continue

        user.age = int(age)
        await user.save()
        print(f"User \"{name}\" ({age}) saved to the database!")


event_loop = asyncio.new_event_loop()
asyncio.set_event_loop(event_loop)
event_loop.run_until_complete(main())
```
